# Golang Template

A template that incorporates the Golang standards recommended [project layout](https://github.com/golang-standards/project-layout).
The project comes with a pipeline that automatically performs linting and testing on every push. If a tag is pushed, new releases
are also created.

## Tooling

### [gotestsum](https://github.com/gotestyourself/gotestsum)

Provides [unit tests reports](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html).

### [gocover-cobertura](https://github.com/t-yuki/gocover-cobertura)

Provides [coverage reports](https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html).

### [golangci-lint](https://golangci-lint.run/)

Provides linting and [codequality reports](https://docs.gitlab.com/ee/ci/testing/code_quality.html).

### [goreleaser](https://goreleaser.com/)

Creates new [releases](https://docs.gitlab.com/ee/user/project/releases/) when a new git tag is pushed.
